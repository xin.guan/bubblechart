import sqlite3
from flask import Flask, render_template, jsonify

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/data')
def get_data():
    conn = sqlite3.connect('../database/bidt_glossar.db')
    cursor = conn.cursor()

    cursor.execute("""
    SELECT *
    FROM WEBSITE_MODULES
    WHERE Title = "Digitale Transformation" AND Level = "Bachelor" 
    AND (Lang = "Deutsch" OR Lang = "Englisch") AND Name_x IS NOT NULL AND Lang IS NOT NULL AND Ects < 25
    ORDER BY Weightedpercentage DESC, Name_x ASC
    LIMIT 40
    """)
    results = cursor.fetchall()

    data_list = []
    for row in results:
        data = {
            'Index': row[0],
            'Name_x': row[1],
            'Uni': row[2],
            'Level': row[3],
            'Lang': row[4],
            'Ects': row[5],
            'Title': row[6],
            'Weightedpercentage': row[7]
        }
        data_list.append(data)

    conn.close()

    return jsonify(data_list)

if __name__ == '__main__':
    app.run(host="127.0.0.1", port=8080, debug=True)



# run
# python3 app.py
# http://127.0.0.1:5000/

# CTRL+C to quit