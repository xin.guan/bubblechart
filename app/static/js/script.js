// Fetch the data from the Flask route
fetch('/data')
    .then(response => response.json())
    .then(data => {
        // Format the data
        const formattedData = data.map(
            d => ({
                name: d.Name_x,
                uni: d.Uni,
                level: d.Level,
                lang: d.Lang,
                ects: d.Ects,
                title: d.Title,
                weightedpercentage: d.Weightedpercentage
            }));

        // Update the chart title dynamically
        const chartTitle = document.getElementById('chartTitle');
        chartTitle.textContent = `Top ${data.length} ${data[0].Level} lectures in ${data[0].Title}`;

        // Calculate the available space within the window
        const width = window.innerWidth;
        const height = window.innerHeight;

        // Adjust the width and height of the SVG container based on available space
        const svgWidth = 0.7 * width;
        const svgHeight = 0.7 * height;

        // Create the SVG container
        const svg = d3.select('#chart')
            .attr('width', svgWidth)
            .attr('height', svgHeight);

        // Create a container for the chart elements
        const chartDiv = d3.select('#chart-div');

        // Define the boundaries of the pan/zoom area
        const minX = -svgWidth / 2;
        const maxX = svgWidth / 2;
        const minY = -svgHeight / 2;
        const maxY = svgHeight / 2;

        // Calculate the center point of the chart
        const centerX = (minX + maxX) / 2;
        const centerY = (minY + maxY) / 2;

        // Define the zoom behavior
        const zoom = d3.zoom()
            .scaleExtent([0.5, 1.5])
            .on('zoom', zoomed);

        chartDiv.call(zoom);

        function zoomed(event) {
            const { transform } = event;
            const { x, y, k } = transform;

            // Calculate the adjusted x and y values to center the zoom around the chart center
            const adjustedX = centerX + (x - centerX) * k;
            const adjustedY = centerY + (y - centerY) * k;

            // Apply transform with restricted boundaries
            // !!!!!!!! THIS KILLS THE LAYOUT!!!!!!!!
            //chartDiv.style('transform', `translate(${adjustedX}px, ${adjustedY}px) scale(${k})`);

            // Apply inverse transform to node positions
            node.attr('transform', d => `translate(${d.x * k + x}, ${d.y * k + y})`);

            // Update bubble size based on zoom scale
            // circle.attr('r', d => d.r * k);
        }

        // Create the SVG container for zoom buttons
        const zoomButtonsContainer = d3.select('.chart-container')
            .append('svg')
            .attr('class', 'zoom-buttons-container')
            .attr('width', 50)
            .attr('height', 200)
            .style('position', 'absolute')
            .style('top', '500')
            .style('left', '50');

        // Add zoom in button
        const zoomInButton = zoomButtonsContainer.append('g')
            .attr('class', 'zoom-button')
            .attr('transform', 'translate(20, 20)')
            .on('click', zoomIn);

        zoomInButton.append('foreignObject')
            .attr('width', 30)
            .attr('height', 30)
            .append('xhtml:div')
            .html('<i class="fas fa-search-plus"></i>');

        // Add zoom out button
        const zoomOutButton = zoomButtonsContainer.append('g')
            .attr('class', 'zoom-button')
            .attr('transform', 'translate(20, 60)')
            .on('click', zoomOut);

        zoomOutButton.append('foreignObject')
            .attr('width', 30)
            .attr('height', 30)
            .append('xhtml:div')
            .html('<i class="fas fa-search-minus"></i>');

        // Add reset button
        const resetButton = zoomButtonsContainer.append('g')
            .attr('class', 'zoom-button')
            .attr('transform', 'translate(20, 100)')
            .on('click', resetZoom);

        resetButton.append('foreignObject')
            .attr('width', 30)
            .attr('height', 30)
            .append('xhtml:div')
            .html('<i class="fas fa-undo"></i>');

        // Function to handle zooming in
        function zoomIn() {
            zoom.scaleBy(chartDiv.transition().duration(750), 1.2);
        }

        // Function to handle zooming out
        function zoomOut() {
            zoom.scaleBy(chartDiv.transition().duration(750), 0.8);
        }

        function resetZoom() {
            chartDiv.transition().duration(750).call(zoom.transform, d3.zoomIdentity);
        }

        const color = d3.scaleOrdinal()
            .domain(['LMU', 'TUM', 'REG', 'JMU', 'AUG'])
            .range([
                'rgba(31, 119, 180, 0.6)',
                'rgba(255, 127, 14, 0.6)',
                'rgba(44, 160, 44, 0.6)',
                'rgba(214, 39, 40, 0.6)',
                'rgba(148, 103, 189, 0.6)'
            ]);

        const hoverColor = d3.scaleOrdinal()
            .domain(['LMU', 'TUM', 'REG', 'JMU', 'AUG'])
            .range([
                'rgba(31, 119, 180, 0.9)',
                'rgba(255, 127, 14, 0.9)',
                'rgba(44, 160, 44, 0.9)',
                'rgba(214, 39, 40, 0.9)',
                'rgba(148, 103, 189, 0.9)'
            ]);

        const bubble = d3.pack(formattedData)
            .size([svgWidth, svgHeight])
            .padding(2);

        const nodes = d3.hierarchy({ children: formattedData })
            .sum(d => d.weightedpercentage);

        const simulation = d3.forceSimulation(nodes.children)
            .force('charge', d3.forceManyBody().strength(-15))
            .force('center', d3.forceCenter(svgWidth / 2, svgHeight / 2))
            .force('collision', d3.forceCollide().radius(d => d.r * 20))
            .force('float', d3.forceManyBody().strength(0.01)) // New force for floating effect
            .on('tick', ticked);


        // Function to update circle positions
        function ticked() {
            const circles = svg.selectAll('.node circle');
            circles.attr('cx', d => d.x / 3.5)
                .attr('cy', d => d.y / 3.5);
        }

        const node = svg.selectAll('.node')
            .data(bubble(nodes).descendants())
            .enter()
            .filter(d => !d.children)
            .append('g')
            .attr('class', 'node')
            .attr('transform', d => `translate(${d.x}, ${d.y})`);

        const circle = node.append('circle')
            .attr('r', d => d.r)
            .style('fill', d => color(d.data.uni))
            .on('mouseover', function () {
                d3.select(this).style('fill', d => hoverColor(d.data.uni));
            })
            .on('mousemove', function () {
                d3.select(this).style('fill', d => hoverColor(d.data.uni));
            })
            .on('mouseout', function () {
                d3.select(this).style('fill', d => color(d.data.uni));
            });

        const tooltip = node.append('foreignObject')
            .attr('class', 'tooltip')
            .attr('width', 200)
            .attr('height', 220);

        tooltip.append('xhtml:div')
            .attr('class', 'tooltip-content')
            .html(d => `
        <div id="tooltip-content"><b>Name:</b> ${d.data.name}</div>
        <div id="tooltip-content"><b>University:</b> ${d.data.uni}</div>
        <div id="tooltip-content"><b>Level:</b> ${d.data.level}</div>
        <div id="tooltip-content"><b>Language:</b> ${d.data.lang}</div>
        <div id="tooltip-content"><b>ECTS:</b> ${d.data.ects}</div>
        <div id="tooltip-content"><b>Title:</b> ${d.data.title}</div>
        <div id="tooltip-content"><b>Weighted Percentage:</b> ${d.data.weightedpercentage}%</div>
    `);

        // Show tooltip on hover
        node.on('mousemove', function (event, d) {
            const [mouseX, mouseY] = d3.pointer(event);

            // Apply the inverse zoom transformation to get the transformed mouse coordinates
            const transform = d3.zoomTransform(chartDiv.node());
            const transformedX = transform.invertX(mouseX);
            const transformedY = transform.invertY(mouseY);

            const tooltipWidth = 200;
            const tooltipHeight = 220;
            const tooltipX = transformedX + tooltipWidth > width ? transformedX - tooltipWidth : transformedX;
            const tooltipY = transformedY + tooltipHeight > height ? transformedY + tooltipHeight : transformedY;

            // Update tooltip position
            d3.select(this)
                .select('.tooltip')
                .style('left', `${tooltipX}px`)
                .style('top', `${tooltipY}px`)
                .style('visibility', 'visible');
        })
            .on('mouseout', function () {
                d3.select(this)
                    .select('.tooltip')
                    .style('visibility', 'hidden');
            });

        // create legend container
        const legendContainer = d3.select('.chart-container')
            .append('div')
            .attr('class', 'legend-container');

        // create legend inside the container
        const legend = legendContainer.selectAll('.legend')
            .data(color.domain())
            .enter()
            .append('div')
            .attr('class', 'legend');

        const legendSvg = legend.append('svg')
            .attr('width', 150)
            .attr('height', 20);

        legendSvg.append('circle')
            .attr('r', 7)
            .attr('cx', 10)
            .attr('cy', 10)
            .style('fill', color);

        legendSvg.append('text')
            .attr('x', 24)
            .attr('y', 14)
            .text(d => d);
    })
    .catch(error => console.log(error));
