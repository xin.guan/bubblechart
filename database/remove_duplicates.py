#!/bin/python3

import sqlite3

con = sqlite3.connect("digital_uni_no_duplicates.db")

cur = con.cursor()
cur2 = con.cursor()
# get the number of modules 
cur.execute("SELECT count(*) FROM modules ")
total = cur.fetchall()[0][0]

# clean up the table 
cur.execute("DELETE FROM modules2  ")
con.commit()

print("%d rows in total" % total)
cur.execute("SELECT * FROM modules order by name ")

old = (None,None,None,None)
rows = cur.fetchall()
i = 0

for row in rows:
	# print progress
	print("%d/%d" % (i,total))
	# we ignore all modules where the title and university are equal. 
	# (IDs might be equal in otherwise similar modules; I suppose this happens
	# when modules are used in multiple study programs)
	if row[1] == old[1] and row[2] == old[2]:
		print("dupe!")
	else:
		sql = "INSERT INTO modules2  VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?) "
		cur2.execute(sql,row)
		con.commit()
	i+=1
	old = row

cur2.close()
cur.close()
con.close()
