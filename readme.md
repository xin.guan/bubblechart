# Bubble Chart with Zoom and Force Layout

This project implements a bubble chart visualization with zoom and force layout using D3.js and Flask for top lecturces in a Title for a Level (Bachelor / Master) for the universities in Bavaria. It fetches data from a Flask route and displays it as interactive bubbles on an HTML page. Users can zoom in and out of the chart and observe the bubbles rearrange themselves based on the force layout simulation.

## Getting Started

To run the project locally, follow these steps:

1. Clone the repository: `git clone https://github.com/your-username/your-repo.git`
2. Install the required dependencies: `pip install -r requirements.txt`
3. go to folder `app´
4. Start the Flask development server: `python app.py`
5. Open your browser and navigate to `http://localhost:5000` to view the chart.

## Features

- Fetches data from a Flask route and formats it for visualization.
- Updates the chart title dynamically based on the data.
- Implements zoom functionality to zoom in and out of the chart.
- Restricts the zoom to the boundaries of the chart container.
- Applies a force layout simulation to arrange the bubbles.
- Updates the bubble positions during zoom and simulation ticks.
- Displays tooltips on hover to show additional information about the bubbles.
- Implements zoom buttons for easy zooming in, zooming out, and resetting zoom.

## Dependencies

The project relies on the following dependencies:

- Flask: A micro web framework for Python. Provides the server-side route to fetch the data.
- D3.js: A JavaScript library for manipulating documents based on data. Used for data visualization and interaction.

## Directory Structure

- `app.py`: The Flask server file that serves the HTML page and provides the data route.
- `templates/index.html`: The HTML file that includes the chart container and necessary scripts.
- `static/css/styles.css`: The CSS file for styling the chart and tooltips.
- `static/js/app.js`: The JavaScript file that contains the D3.js code for creating the bubble chart and handling zoom and force layout.

## Data Format

The data is fetched from the Flask route and should be in the following format:

```json
[
  {
    "Name_x": "Bubble 1",
    "Uni": "LMU",
    "Level": "Bachelor",
    "Lang": "Englisch",
    "Ects": 6,
    "Title": "Digitale Transformation",
    "Weightedpercentage": 80
  },
  {
    "Name_x": "Bubble 2",
    "Uni": "TUM",
    "Level": "Master",
    "Lang": "Deutsch",
    "Ects": 4,
    "Title": "Digitale Transformation",
    "Weightedpercentage": 60
  },
   {
    "Name_x": "Bubble 3",
    "Uni": "JMU",
    "Level": "Master",
    "Lang": "Deutsch/Englisch",
    "Ects": 5,
    "Title": "Digitale Transformation",
    "Weightedpercentage": 40
  },
  ...
]
```

## Feedback and Contributions

Feedback and contributions to improve this project are always welcome. If you encounter any issues or have suggestions for enhancements, please create an issue or submit a pull request.
